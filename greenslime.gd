extends Node2D


var time = 0.0


func _process(delta):
	time += delta*10
	var skeleton = get_node("/root/greenslime/Skeleton2D")
	var lleg = skeleton.get_children()[0].get_children()[0]
	var rleg = skeleton.get_children()[0].get_children()[1]
	var mleg = skeleton.get_children()[0].get_children()[3]
	var bottom = skeleton.get_children()[0].get_children()[4]
	var lleg_transform = lleg.get_transform()
	var rleg_transform = rleg.get_transform()
	var mleg_transform = rleg.get_transform()
	var bottom_transform = bottom.get_transform()
	var a = 5.0
	var b = 0.0
	lleg_transform.origin += Vector2(a*sin(time), -a*cos(time))
	rleg_transform.origin += Vector2(a*sin(time), -a*cos(time))
	mleg_transform.origin += Vector2(a*sin(time), -a*cos(time))
	bottom_transform.origin += Vector2(a*sin(time), -a*cos(time))
	lleg.set_transform(lleg_transform)
	rleg.set_transform(rleg_transform)
	mleg.set_transform(mleg_transform)
	bottom.set_transform(bottom_transform)
	